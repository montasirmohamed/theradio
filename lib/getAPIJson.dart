// Function to get the JSON data
Future<String> getJSONData() async {
  var response = await http.get(
    // Encode the url
      Uri.encodeFull("https://unsplash.com/napi/photos/Q14J2k8VE3U/related"),
      // Only accept JSON response
      headers: {"Accept": "application/json"}
  );

  setState(() {
    // Get the JSON data
    data = json.decode(response.body)['results'];
  });

  return "Successfull";
}